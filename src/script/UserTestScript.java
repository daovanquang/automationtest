/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package script;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import static org.openqa.grid.common.SeleniumProtocol.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.domain.UserLoginTestcase;
import selenium.domain.UserRegisterTestcase;

/**
 *
 * @author rabbit
 */
public class UserTestScript {
    
    public static void login(List<UserLoginTestcase> list, String chromePath, String url){
        //declare driver
        System.setProperty("webdriver.chrome.driver",chromePath);
        WebDriver driver = new ChromeDriver();
        
        //open page
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
        
        //click account button 
        WebElement accountBtn = driver.findElement(By.id("account-menu"));
        accountBtn.click();
        
        //click login button
        WebElement loginBtn = driver.findElement(By.id("login"));
        loginBtn.click();

        for(UserLoginTestcase item:list){
            //type login
            WebElement txtLogin = driver.findElement(By.id("username"));
            txtLogin.sendKeys(item.getLogin());

            //type password
            WebElement txtPassword = driver.findElement(By.id("password"));
            txtPassword.sendKeys(item.getPassword());
            
            //submit
            WebElement submitBtn = driver.findElement(By.cssSelector("button.btn.btn-primary"));
            submitBtn.click();
            
            //wait unti show alert text
            WebDriverWait wait = new WebDriverWait(driver, 10);
            
            //get alert text
            WebElement alertText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.alert.alert-danger")));
            List<WebElement> strong = alertText.findElements(By.tagName("strong"));
            
            //check result
            if(strong.get(0).getText().equalsIgnoreCase(item.getOutput())){
                System.out.println("pass");
            }else{
                System.out.println("fail");
            }
            
            //reset input
            txtLogin.clear();
            txtPassword.clear();
        }
        
        driver.close();
    }
    
     public static void register(List<UserRegisterTestcase> list, String chromePath, String url){
         //declare driver
        System.setProperty("webdriver.chrome.driver",chromePath);
        WebDriver driver = new ChromeDriver();
        
        //open page
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 

        WebElement txtLogin = driver.findElement(By.name("login"));;
        WebElement txtEmail = driver.findElement(By.name("email"));;
        WebElement txtPassword = driver.findElement(By.name("password"));;
        WebElement txtRetypePassword = driver.findElement(By.name("confirmPassword"));
        
        String KEY_ANY = "a";
        
        for(UserRegisterTestcase item:list){
            
            if(item.getLogin()!=null)
                txtLogin.sendKeys(item.getLogin());
            else{
                txtLogin.sendKeys(KEY_ANY);
                txtLogin.sendKeys(Keys.BACK_SPACE);
            }
            
            if(item.getEmail()!=null)
                txtEmail.sendKeys(item.getEmail());
            else{
                txtEmail.sendKeys(KEY_ANY);
                txtEmail.sendKeys(Keys.BACK_SPACE);
            }
            
            if(item.getPassword()!=null)
                txtPassword.sendKeys(item.getPassword());
            else{
                txtPassword.sendKeys(KEY_ANY);
                txtPassword.sendKeys(Keys.BACK_SPACE);
            }
            
            if(item.getRetypePassword()!=null)
                txtRetypePassword.sendKeys(item.getRetypePassword());
            else{
                txtRetypePassword.sendKeys(KEY_ANY);
                txtRetypePassword.sendKeys(Keys.BACK_SPACE);
            }
            
            //get alerts
            List<WebElement> formGroups = driver.findElements(By.cssSelector("div.form-group"));
        
            //wait unti show alert text
            WebDriverWait wait = new WebDriverWait(driver, 20);

            WebElement loginAlert = null;
            WebElement emailAlert = null;
            WebElement passwordAlert = null;
            WebElement retypePasswordAlert = null;
            
            if(item.getLoginAlert()!=null)
                loginAlert = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(formGroups.get(0), By.cssSelector("small.form-text.text-danger")));
            
            if(item.getEmailAlert()!=null)
                emailAlert = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(formGroups.get(1), By.cssSelector("small.form-text.text-danger")));
            
            if(item.getPasswordAlert()!=null)
                passwordAlert = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(formGroups.get(2), By.cssSelector("small.form-text.text-danger")));
           
            if(item.getRetypePasswordAlert()!=null)
                retypePasswordAlert = wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(formGroups.get(3), By.cssSelector("small.form-text.text-danger")));
            
            //checkresult
            boolean checkAlert = (loginAlert != null && item.getLoginAlert().equalsIgnoreCase(loginAlert.getText()))
                    || (emailAlert!=null && item.getEmailAlert().equalsIgnoreCase(emailAlert.getText()))
                    || (passwordAlert!=null && item.getPasswordAlert().equalsIgnoreCase(passwordAlert.getText()))
                    || (retypePasswordAlert!=null && item.getRetypePasswordAlert().equalsIgnoreCase(retypePasswordAlert.getText()));
            
            if(checkAlert){
                System.out.println("pass");
            }else{
                System.out.println("fail");
            }
            
            txtLogin.clear();
            txtEmail.clear();
            txtPassword.clear();
            txtRetypePassword.clear();
            
//            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        }
        
        driver.close();
     }
}
