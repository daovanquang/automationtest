/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package selenium.domain;

/**
 *
 * @author rabbit
 */
public class UserRegisterTestcase {
    private String login;
    private String email;
    private String password;
    private String retypePassword;
    private String loginAlert;
    private String emailAlert;
    private String passwordAlert;
    private String retypePasswordAlert;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String getLoginAlert() {
        return loginAlert;
    }

    public void setLoginAlert(String loginAlert) {
        this.loginAlert = loginAlert;
    }

    public String getEmailAlert() {
        return emailAlert;
    }

    public void setEmailAlert(String emailAlert) {
        this.emailAlert = emailAlert;
    }

    public String getPasswordAlert() {
        return passwordAlert;
    }

    public void setPasswordAlert(String passwordAlert) {
        this.passwordAlert = passwordAlert;
    }

    public String getRetypePasswordAlert() {
        return retypePasswordAlert;
    }

    public void setRetypePasswordAlert(String retypePasswordAlert) {
        this.retypePasswordAlert = retypePasswordAlert;
    }

    @Override
    public String toString() {
        return "UserRegisterTestcase{" + "login=" + login + ", email=" + email + ", password=" + password + ", retypePassword=" + retypePassword + ", loginAlert=" + loginAlert + ", emailAlert=" + emailAlert + ", passwordAlert=" + passwordAlert + ", retypePasswordAlert=" + retypePasswordAlert + '}';
    }
    
}
