package selenium.domain;

public class UserLoginTestcase {
    private String login;
    private String password;
    private Boolean actived;
    private String result;
    private String output;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActived() {
        return actived;
    }

    public void setActived(Boolean actived) {
        this.actived = actived;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    @Override
    public String toString() {
        return "UserTestcase{" + "login=" + login + ", password=" + password + ", actived=" + actived + ", result=" + result + ", output=" + output + '}';
    }
}
