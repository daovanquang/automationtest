package selenium.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import selenium.domain.UserLoginTestcase;
import selenium.domain.UserRegisterTestcase;

public class ExcelUtil {

    //get sheet from excel
    public static Sheet getSheet(String path, String sheetName){
       
        try {
            Workbook wb = new XSSFWorkbook(new FileInputStream(path));
            
            if(wb == null) return null;
            
            Sheet sheet = sheetName!=null ? (XSSFSheet) wb.getSheet(sheetName) : (XSSFSheet) wb.getSheetAt(0);
            wb.close();
            return sheet;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelUtil.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public static List<UserLoginTestcase> toUserLoginTestcase(String path, String sheetName, int startIndex){
        List<UserLoginTestcase> list = new ArrayList<>();

        Sheet sheet = getSheet(path, sheetName);
        
        if(sheet==null) return list;
        
        try{
            int index = 0;

            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                
                if(index>=startIndex){
                    UserLoginTestcase testcase = new UserLoginTestcase();
                    Cell loginCell = row.getCell(1);
                    Cell passwordCell = row.getCell(2);
                    Cell activedCell = row.getCell(3);
                    Cell outputCell = row.getCell(4);
                    Cell resultCell = row.getCell(5);
                    
                    if(loginCell!=null)
                        testcase.setLogin(loginCell.getStringCellValue());
                    
                    if(passwordCell!=null)
                    testcase.setPassword(passwordCell.getStringCellValue());
                    
                    if(activedCell!=null){
                        Double actived = activedCell.getNumericCellValue();
                        testcase.setActived(actived==0? false : actived==1 ? true : null);
                    }
                    
                    if(outputCell!=null)
                        testcase.setOutput(outputCell.getStringCellValue());
                    
                    if(resultCell!=null)
                        testcase.setResult(resultCell.getStringCellValue());
                    
                    list.add(testcase);
                }
                
                index++;
            }
            
        }catch(Exception ex){
            Logger.getLogger(ExcelUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
    
    public static List<UserRegisterTestcase> toUserRegisterTestcase(String path, String sheetName, int startIndex){
        List<UserRegisterTestcase> list = new ArrayList<>();

        Sheet sheet = getSheet(path, sheetName);
        
        if(sheet==null) return list;
        
        try{
            int index = 0;

            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {

                Row row = rowIterator.next();
                
                if(index>=startIndex){
                    UserRegisterTestcase testcase = new UserRegisterTestcase();
                    Cell loginCell = row.getCell(1);
                    Cell emailCell = row.getCell(2);
                    Cell passwordCell = row.getCell(3);
                    Cell retypePasswordCell = row.getCell(4);
                    Cell loginAlertCell = row.getCell(5);
                    Cell emailAlertCell = row.getCell(6);
                    Cell passwordAlertCell = row.getCell(7);
                    Cell retypePasswordAlertCell = row.getCell(8);
                    
                    if(loginCell!=null)
                        testcase.setLogin(loginCell.getStringCellValue());
                    
                     if(emailCell!=null)
                        testcase.setEmail(emailCell.getStringCellValue());
                    
                    if(passwordCell!=null)
                        testcase.setPassword(passwordCell.getStringCellValue());
                    
                    if(retypePasswordCell!=null){
                        testcase.setRetypePassword(retypePasswordCell.getStringCellValue());
                    }
                    
                    if(loginAlertCell!=null)
                        testcase.setLoginAlert(loginAlertCell.getStringCellValue());
                    
                    if(emailAlertCell!=null)
                        testcase.setEmailAlert(emailAlertCell.getStringCellValue());
                    
              
                     if(passwordAlertCell!=null)
                        testcase.setPasswordAlert(passwordAlertCell.getStringCellValue());
                    
                    if(retypePasswordAlertCell!=null)
                        testcase.setRetypePasswordAlert(retypePasswordAlertCell.getStringCellValue());
                    
                    list.add(testcase);
                }
                
                index++;
            }
            
        }catch(Exception ex){
            Logger.getLogger(ExcelUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }
}
