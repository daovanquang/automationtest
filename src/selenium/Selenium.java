/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package selenium;

import java.util.List;
import script.UserTestScript;
import selenium.domain.UserLoginTestcase;
import selenium.domain.UserRegisterTestcase;
import selenium.utils.ExcelUtil;

/**
 *
 * @author rabbit
 */
public class Selenium {

    private static final String chromePath = "D:\\TechAsians\\Selenium\\selenium\\browsers\\chromedriver.exe";
    private static final String url = "http://27.72.105.53:7979/#/register";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//      List<UserLoginTestcase> list = ExcelUtil.toUserLoginTestcase("D:\\TechAsians\\Selenium\\selenium\\resources\\userlogin.xlsx", null, 1);
//      UserTestScript.login(list, chromePath, url);

        List<UserRegisterTestcase> list = ExcelUtil.toUserRegisterTestcase("D:\\TechAsians\\Selenium\\selenium\\resources\\userregister.xlsx", null, 1);
        UserTestScript.register(list, chromePath, url);
    }
    
}
